<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    use HasFactory;

    protected $table = 'accounts';

    public $timestamps = true;

    protected $guarded = [];

    static $rules = [
        'name' => 'required|string',
        'owner_id' => 'required|exists:users,id',
        'address' => 'required|string',
        'town_city' => 'required|string',
        'country' => 'required|string',
        'post_code' => 'required|string',
        'phone' => 'required|string',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class);
    }

    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    public function deletecheck(): array
	{

		$r = [
			'can' => true,
			'message' => null
		];

		$count_contacts = $this->contacts()->count();
		if($count_contacts)
		{
			$r['can'] = false;
			$r['message'] = 'Please delete all contacts relating to this account before deleting an account.';
			return $r;
		}

		return $r;
	}
}
