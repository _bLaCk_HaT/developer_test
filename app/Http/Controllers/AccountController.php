<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Inertia\Inertia;


class AccountController extends Controller
{
    public function index()
    {
        //All accounts
        $accounts = Account::select('id', 'name', 'country', 'town_city', 'phone')->orderBy('name', 'asc')->get();

        //Auth user related accounts
        //$accounts = $user->accounts()->select('id', 'name', 'country', 'town_city', 'phone')->orderBy('name', 'asc')->get();

        return Inertia::render('Accounts/Index', ['accounts' => $accounts]);
        //return inertia('Accounts/Index', ['accounts' => $accounts]);
    }

    public function show(string $id)
    {
        $account = Account::with('contacts')->findOrFail($id);

		return Inertia::render('Accounts/Show', ['account' => $account]);
        //return inertia('Accounts/Show', ['accounts' => $accounts]);
    }

    public function create()
    {
        $users = User::select('id', 'name')->get();
		
		return Inertia::render('Accounts/Create', ['users' => $users]);
        //return inertia('Accounts/Create', ['users' => $users]);
    }

    public function store(Request $request)
    {
        $request->validate(Account::$rules);
        $accounts = Account::select('name', 'address', 'country', 'town_city')->get();
			foreach($accounts as $account)
				if($account->name==$request->name && $account->address==$request->address && $account->country==$request->country && $account->town_city==$request->town_city)
					throw ValidationException::withMessages(['name' => 'Account name already exists for this country, city and address']);

        $account = new Account;
		$account->name = $request->name;
		$account->owner_id = (int)$request->owner_id;
		$account->address = $request->address;
		$account->town_city = $request->town_city;
		$account->country = $request->country;
		$account->post_code = $request->post_code;
		$account->phone = $request->phone;
		$account->save();

        return redirect()->route('accounts.index')->with('success', 'Successfully created '.$account->name.' account');
    }

    public function edit(Account $account)
    {
        $users = User::select('id', 'name')->get();

		return Inertia::render('Accounts/Edit', ['account' => $account, 'users' => $users]);
        //return inertia('Accounts/Edit', ['account' => $account, 'users' => $users]);
    }

    public function update(Request $request, Account $account)
    {
        $request->validate(Account::$rules);
        $accounts = Account::select('id', 'name', 'address', 'country', 'town_city')->get();
			foreach($accounts as $a)
				if($a->id!=$account->id && $a->name==$account->name && $a->address==$account->address && $a->country==$account->country && $a->town_city==$account->town_city)
					throw ValidationException::withMessages(['name' => 'Account name already exists for this country, city and address']);

        $account->update([
            'name' => $request->name,
            'owner_id' => (int)$request->owner_id,
            'address' => $request->address,
            'town_city' => $request->town_city,
            'country' => $request->country,
            'post_code' => $request->post_code,
            'phone' => $request->phone
        ]);

        return redirect()->route('accounts.index')->with('success', 'Successfully updated '.$account->name.' account');
    }

    public function destroy(Account $account)
    {
        $deletecheck = $account->deletecheck();
		if(!$deletecheck['can'])
			return redirect()->route('accounts.index')->with('error', $deletecheck['message']);

        $account->delete();

        return redirect()->route('accounts.index')->with('success', 'Successfully deleted '.$account->name.' account');
    }
}