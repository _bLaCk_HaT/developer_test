<?php

namespace App\Http\Controllers;

use App\Models\Account;
use App\Models\Contact;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ContactController extends Controller
{
    public function index()
    {
        //All contacts
        $contacts = Contact::select('id', 'first_name', 'last_name', 'email', 'phone', 'account_id', 'position')
            ->with(['account' => function($query){
                $query->select('id', 'name');
            }])
            ->orderBy('last_name', 'asc')
            ->get();

        return Inertia::render('Contacts/Index', ['contacts' => $contacts]);
        //return inertia('Contacts/Index', ['contacts' => $contacts]);
    }

    public function show(string $id)
    {
        $contact = Contact::with('account')->findOrFail($id);

		return Inertia::render('Contacts/Show', ['contact' => $contact]);
        //return inertia('Contacts/Show', ['contact' => $contact]);
    }

    public function create()
    {
        $accounts = Account::select('id', 'name')->get();
		
		return Inertia::render('Contacts/Create', ['accounts' => $accounts]);
        //return inertia('Contacts/Create', ['accounts' => $accounts]);
    }

    public function store(Request $request)
    {
        $request->validate(Contact::$rules);
        $request->validate([
			'email' => 'required|unique:contacts,email',
		]);

        $contact = new Contact;
		$contact->first_name = $request->first_name;
		$contact->last_name = $request->last_name;
        $contact->account_id = (int)$request->account_id;
		$contact->email = $request->email;
		$contact->phone = $request->phone;
        $contact->position = $request->position;
		$contact->save();

        return redirect()->route('contacts.index')->with('success', 'Successfully created '.$contact->first_name.' '.$contact->last_name.' contact');
    }

    public function edit(Contact $contact)
    {
        $accounts = Account::select('id', 'name')->get();

		return Inertia::render('Contacts/Edit', ['contact' => $contact, 'accounts' => $accounts]);
        //return inertia('Contacts/Edit', ['contact' => $contact, 'accounts' => $accounts]);
    }

    public function update(Request $request, Contact $contact)
    {
        $request->validate(Contact::$rules);
        $request->validate([
			'email' => 'required|unique:contacts,email,'.$contact->id,
		]);

        $contact->update([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'account_id' => (int)$request->account_id,
            'email' => $request->email,
            'phone' => $request->phone,
            'position' => $request->position,
        ]);

        return redirect()->route('contacts.index')->with('success', 'Successfully updated '.$contact->first_name.' '.$contact->last_name.' contact');
    }

    public function destroy(Contact $contact)
    {
        $contact->delete();

        return redirect()->route('contacts.index')->with('success', 'Successfully deleted '.$contact->first_name.' '.$contact->last_name.' contact');
    }
}
